﻿namespace MagicNumbers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Program
    {
        public static void Main(string[] args)
        {
            var service = new FooService();

            var modelType = ModelType.Complex;

            var l1 = service.Load(3);
            var l2 = service.Load(modelType);
            var l3 = service.Load(ModelType.Complex);

            var m1 = l1.Where(foo => foo.ModelType == (ModelType)3);
            var m2 = l2.Where(foo => foo.ModelType == modelType);
            var m3 = l3.Where(foo => foo.ModelType == ModelType.Complex);
        }
    }

    public class FooObject
    {
        public object Data { get; set; }

        public ModelType ModelType { get; set; }
    }

    public interface IFooService
    {
        List<FooObject> Load(int modelType);
        List<FooObject> Load(ModelType type);
    }

    public class FooService : IFooService
    {
        public List<FooObject> Load(int modelType)
        {
            throw new NotImplementedException();
        }

        public List<FooObject> Load(ModelType type)
        {
            throw new NotImplementedException();
        }
    }

    public enum ModelType
    {
        Unknown = 0,
        Simple,
        Complex
    }
}
