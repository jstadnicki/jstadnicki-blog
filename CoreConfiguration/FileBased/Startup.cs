﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace FileBased
{
    using Microsoft.Extensions.Configuration;

    public class Startup
    {
        private IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Run(async (context) =>
            {
                System.Diagnostics.Trace.WriteLine(this.configuration.GetValue<string>("Common"));
                System.Diagnostics.Trace.WriteLine(this.configuration.GetValue<string>("EnviromentSpecific"));
                await context.Response.WriteAsync("Hello World!");
            });
        }
    }
}
