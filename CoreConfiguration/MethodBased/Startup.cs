﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace MethodBased
{
    using Microsoft.Extensions.Configuration;

    public class Startup
    {
        private IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            System.Diagnostics.Trace.WriteLine("Default");
        }

        public void ConfigureOstrapilaServices(IServiceCollection services)
        {
            System.Diagnostics.Trace.WriteLine("ostrapila.pl");
        }

        public void ConfigureStadnickiServices(IServiceCollection services)
        {
            System.Diagnostics.Trace.WriteLine("jaroslawstadnicki.pl");
        }

        public void ConfigureProductionServices(IServiceCollection services)
        {
            System.Diagnostics.Trace.WriteLine("isthereanynews.pl");
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Run(async (context) =>
                    {
                        System.Diagnostics.Trace.WriteLine(this.configuration.GetValue<string>("Common"));
                        System.Diagnostics.Trace.WriteLine(this.configuration.GetValue<string>("EnviromentSpecific"));
                        await context.Response.WriteAsync("Hello World!");
                    });
        }
    }
}
