﻿namespace IOCHijack.Models
{
    public enum IOCStatus
    {
        Invalid=0,
        Hijacked,
        Normal
    }
}