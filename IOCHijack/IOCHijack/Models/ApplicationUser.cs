namespace IOCHijack.Models
{
    public class ApplicationUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}