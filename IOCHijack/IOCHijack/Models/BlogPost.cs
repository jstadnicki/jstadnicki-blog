﻿using System;

namespace IOCHijack.Models
{
    public class BlogPost
    {
        public BlogPost():this(-1,string.Empty,DateTime.MinValue)
        {}

        public BlogPost(long authorId, string text, DateTime created)
        {
            this.AuthorId = authorId;
            this.Text = text;
            this.Created = created;
        }

        public long Id { get; set; }
        public long AuthorId { get; set; }
        public string Text { get; set; }
        public DateTime Created { get; set; }
    }
}