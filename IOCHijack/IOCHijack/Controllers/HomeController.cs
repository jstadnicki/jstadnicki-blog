﻿using System.Web.Mvc;
using IOCHijack.Dtos;
using IOCHijack.Services;
using IOCHijack.ViewModels;

namespace IOCHijack.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAccountService accountService;
        private readonly IPostingService postingService;
        private readonly ITestService testService;
        private readonly IHijackService hijackService;

        public HomeController(
            IAccountService accountService,
            IPostingService postingService,
            ITestService testService,
            IHijackService hijackService)
        {
            this.accountService = accountService;
            this.postingService = postingService;
            this.testService = testService;
            this.hijackService = hijackService;
        }


        public ActionResult Index()
        {
            var posts = this.postingService.GetAllPosts();
            var iocStatus = this.hijackService.GetStatus();
            var viewModel = new IndexViewModel(posts, iocStatus);
            return this.View("Index", viewModel);
        }

        [HttpGet]
        public ActionResult Register()
        {
            var viewmodel = new RegisterViewModel();
            return this.View("Register", viewmodel);
        }

        [HttpPost]
        public ActionResult Register(RegisterDto dto)
        {
            this.accountService.Register(dto);
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Login()
        {
            var viewmodel = new LoginViewModel();
            return this.View("Login", viewmodel);
        }

        [HttpPost]
        public ActionResult Login(LoginDto dto)
        {
            this.accountService.Login(dto);
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public ActionResult NewBlogPost()
        {
            var viewmodel = new BlogPostViewModel();
            return this.View("NewBlogPost", viewmodel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult NewBlogPost(NewBlogPostDto dto)
        {
            this.postingService.NewBlogPost(dto);
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public ActionResult Logout()
        {
            this.accountService.Logout();
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Hijack()
        {
            this.hijackService.HijackIOC();
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Restore()
        {
            this.hijackService.RestoreIOC();
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult NewUsers()
        {
            this.testService.CreateNewUsers();
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult NewPosts()
        {
            this.testService.CreateNewPosts();
            return this.RedirectToAction("Index");
        }
    }
}