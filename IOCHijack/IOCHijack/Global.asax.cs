﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using IOCHijack.Services;
using WebMatrix.WebData;

namespace IOCHijack
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ApplicationIocContainer.Initialize();

            WebSecurity.InitializeDatabaseConnection("DefaultConnection", "ApplicationUsers", "Id", "Name", false);

        }
    }
}
