﻿using System.Collections.Generic;
using IOCHijack.Models;

namespace IOCHijack.ViewModels
{
    public class IndexViewModel
    {
        public List<BlogPost> Posts { get; set; }
        public IOCStatus IOCStatus { get; set; }

        public IndexViewModel(List<BlogPost> posts, IOCStatus iocStatus)
        {
            this.Posts = posts;
            this.IOCStatus = iocStatus;
        }
    }
}