﻿namespace IOCHijack.Dtos
{
    public class NewBlogPostDto
    {
        public string Text { get; set; }
    }
}