﻿using System.Data.Entity;
using IOCHijack.Models;

namespace IOCHijack.Services
{
    public class ApplicationDatabase : DbContext
    {
        public ApplicationDatabase()
            : base("DefaultConnection")
        {}

        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Membership> Memberships { get; set; }
        public DbSet<OAuthMembership> OAuthMemberships { get; set; }
        public DbSet<UsersInRole> UsersInRoles { get; set; }
        public DbSet<ApplicationUser> Users{ get; set; }
    }
}