using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace IOCHijack.Services
{
    public static class ApplicationIocContainer
    {
        public static IContainer Container { get; private set; }

        public static void Initialize()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<WebSecurityAccountService>().As<IAccountService>();
            builder.RegisterType<PostingService>().As<IPostingService>();
            builder.RegisterType<TestService>().As<ITestService>();
            builder.RegisterType<HijackService>().As<IHijackService>();

            builder.RegisterType<ApplicationDatabase>().InstancePerLifetimeScope();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            Container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));
        }
    }
}