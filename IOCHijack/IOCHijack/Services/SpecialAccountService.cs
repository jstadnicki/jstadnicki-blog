﻿using IOCHijack.Dtos;

namespace IOCHijack.Services
{
    public class SpecialAccountService : IAccountService
    {
        private readonly WebSecurityAccountService realAccountService;

        public SpecialAccountService()
        {
            this.realAccountService = new WebSecurityAccountService();
        }

        public void Logout()
        {
            this.realAccountService.Logout();
        }

        public bool Login(LoginDto dto)
        {
            return this.realAccountService.Login(dto);
        }

        public void Register(RegisterDto dto)
        {
            this.realAccountService.Register(dto);
        }

        static public int CurrentLoggedUserId { get; set; }

        public int GetCurrentLoggedUserId()
        {
            return CurrentLoggedUserId;
        }
    }
}