﻿using System.Linq;
using IOCHijack.Dtos;

namespace IOCHijack.Services
{
    public class TestService : ITestService
    {
        private readonly ApplicationDatabase database;
        private readonly IAccountService accountService;
        private readonly IPostingService postingService;

        public TestService(ApplicationDatabase database, IAccountService accountService, IPostingService postingService)
        {
            this.database = database;
            this.accountService = accountService;
            this.postingService = postingService;
        }

        public void CreateNewPosts()
        {
            var users = this.database.Users.ToList();
            var random = users.ElementAt(Faker.NumberFaker.Number(users.Count - 1));
            var dto = new NewBlogPostDto
            {
                Text = Faker.TextFaker.Sentences(3)
            };

            SpecialAccountService.CurrentLoggedUserId = random.Id;

            this.postingService.NewBlogPost(dto);
        }

        public void CreateNewUsers()
        {
            var dto = new RegisterDto
            {
                Email = Faker.InternetFaker.Email() + Faker.NumberFaker.Number(),
                Password = Faker.NumberFaker.Number(1000000000, 2000000000).ToString()
            };
            this.accountService.Register(dto);
        }
    }
}