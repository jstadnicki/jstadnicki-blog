using System;
using System.Collections.Generic;
using System.Linq;
using IOCHijack.Dtos;
using IOCHijack.Models;

namespace IOCHijack.Services
{
    public class PostingService : IPostingService
    {
        private readonly ApplicationDatabase database;
        private readonly IAccountService accountService;

        public PostingService(ApplicationDatabase database, IAccountService accountService)
        {
            this.database = database;
            this.accountService = accountService;
        }

        public void NewBlogPost(NewBlogPostDto dto)
        {
            var userId = this.accountService.GetCurrentLoggedUserId();
            var model = new BlogPost(userId, dto.Text, DateTime.Now);
            this.database.BlogPosts.Add(model);
            this.database.SaveChanges();
        }

        public List<BlogPost> GetAllPosts()
        {
            var result = this.database.BlogPosts.OrderBy(o => o.Created).ToList();
            return result;
        }
    }
}