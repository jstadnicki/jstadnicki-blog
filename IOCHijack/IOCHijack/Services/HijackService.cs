﻿using Autofac;
using IOCHijack.Models;

namespace IOCHijack.Services
{
    public class HijackService : IHijackService
    {
        public void RestoreIOC()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<WebSecurityAccountService>().As<IAccountService>();
            builder.Update(ApplicationIocContainer.Container);
        }

        public void HijackIOC()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<SpecialAccountService>().As<IAccountService>().SingleInstance();
            builder.Update(ApplicationIocContainer.Container);
        }

        public IOCStatus GetStatus()
        {
            var isHijacked = ApplicationIocContainer.Container.Resolve<IAccountService>() is SpecialAccountService;
            return isHijacked ? IOCStatus.Hijacked : IOCStatus.Normal;
        }
    }
}