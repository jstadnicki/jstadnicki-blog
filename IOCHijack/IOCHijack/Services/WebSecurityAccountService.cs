using IOCHijack.Dtos;
using WebMatrix.WebData;

namespace IOCHijack.Services
{
    class WebSecurityAccountService : IAccountService
    {
        public void Logout()
        {
            WebSecurity.Logout();
        }

        public bool Login(LoginDto dto)
        {
            var result = WebSecurity.Login(dto.Email, dto.Password);
            return result;
        }

        public void Register(RegisterDto dto)
        {
            WebSecurity.CreateUserAndAccount(
                dto.Email,
                dto.Password);
        }

        public int GetCurrentLoggedUserId()
        {
            return WebSecurity.CurrentUserId;
        }
       
    }
}