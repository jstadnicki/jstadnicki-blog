﻿using System.Collections.Generic;
using IOCHijack.Dtos;
using IOCHijack.Models;

namespace IOCHijack.Services
{
    public interface IPostingService
    {
        void NewBlogPost(NewBlogPostDto dto);
        List<BlogPost> GetAllPosts();
    }
}