﻿using IOCHijack.Models;

namespace IOCHijack.Services
{
    public interface IHijackService
    {
        void RestoreIOC();
        void HijackIOC();
        IOCStatus GetStatus();
    }
}