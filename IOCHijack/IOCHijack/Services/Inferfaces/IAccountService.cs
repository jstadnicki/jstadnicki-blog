using IOCHijack.Dtos;

namespace IOCHijack.Services
{
    public interface IAccountService
    {
        void Logout();
        bool Login(LoginDto dto);
        void Register(RegisterDto dto);
        int GetCurrentLoggedUserId();
    }
}