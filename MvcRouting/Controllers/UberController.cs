﻿namespace MvcRouting.Controllers
{
    using System.Linq;
    using System.Web.Mvc;

    public class UberController : Controller
    {
        public string Index()
        {
            var x = this.RouteData.Values;
            return string.Join("\n", x.Select(t => string.Join(":", t.Key, t.Value)));
        }
    }
}