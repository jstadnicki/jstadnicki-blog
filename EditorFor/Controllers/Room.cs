﻿using System;

namespace EditorFor.Controllers
{
    public class Room
    {
        public long Id { get; set; }
        public string RoomName { get; set; }
        public int BedsCount { get; set; }
        public bool SmokingAllowed { get; set; }
    }
}