﻿using System.Collections.Generic;

namespace EditorFor.Controllers
{
    public class Hotel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public Address Adress{ get; set; }
        public List<Room> Rooms { get; set; }
    }
}