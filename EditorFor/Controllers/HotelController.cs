﻿using System.Web.Mvc;

namespace EditorFor.Controllers
{
    public class HotelController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            var viewmodel = new NewPostViewModel();
            return new EmptyResult();
        }

        public ActionResult Create(NewPostDto model)
        {
            return new EmptyResult();
        }

        public ActionResult Edit()
        {
            var viewmodel = new EditPostViewModel();
            return new EmptyResult();
        }

        public ActionResult Edit(EditPostDto model)
        {
            return new EmptyResult();
        }


    }
}