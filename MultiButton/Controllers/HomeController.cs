﻿using System.Web.Mvc;

namespace MultiButton.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var userViewModel = new UserViewModel();
            return View("Index", userViewModel);
        }

        [HttpPost]
        public ActionResult Index(UserDto dto)
        {
            if (dto.Action == FormAction.Prev)
            {
                return RedirectToAction("Index");
            }

            return View("FormAction", dto);
        }
    }

    public class UserViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public bool Active { get; set; }
    }

    public class UserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public bool Active { get; set; }
        public FormAction Action { get; set; }
    }

    public enum FormAction
    {
        Unknown = 0,
        Next = 1,
        Prev = 2
    }
}