﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MultiButton.Startup))]
namespace MultiButton
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
