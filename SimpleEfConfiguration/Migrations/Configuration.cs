using System.Collections.Generic;
using SimpleEFConfiguration;

namespace SimpleEfConfiguration.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SimpleEFConfiguration.SimpleDatabase>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SimpleEFConfiguration.SimpleDatabase context)
        {
            var person = new Person() {Name = "Janek"};
            person.Properties = new List<Property>();
            person.Properties.Add(new Property {Name = "Si�a"});

            context.Persons.Add(person);
            context.SaveChanges();
        }
    }
}
