﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SimpleEFConfiguration
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = new SimpleDatabase();
            var p1 = db.Persons.ToList();
            var p2 = db.Persons.Include(person => person.Properties).ToList();
        }
    }

    public class SimpleDatabase : DbContext
    {
        public SimpleDatabase()
        {
            this.Database.Log = text => System.Diagnostics.Debug.WriteLine(text);
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Property> Properties { get; set; }
        public DbSet<Person> Persons { get; set; }
    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Property> Properties { get; set; }
    }

    public class Property
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PersonId { get; set; }
    }
}
