using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomAuthentication.Controllers
{
    public class InMemoryCustomDatabase : ICustomDatabase
    {
        static List<CustomUser> users = new List<CustomUser>();

        public Task<CustomUser> FindCustomUserByNameAsync(string user)
        {
            return Task.Run(() => users.FirstOrDefault(x => x.UserName == user));
        }

        public Task<CustomUser> FindCustomUserByIdAsync(string id)
        {
            return Task.Run(() => users.FirstOrDefault(x => x.Id == id));
        }

        public Task DeleteCustomUserAsync(CustomUser user)
        {
            return Task.Run(() => users.Remove(user));
        }

        public Task UpdateCustomUserAsync(CustomUser user)
        {
            return Task.Run(() =>
            {
                var firstOrDefault = users.FirstOrDefault(x => x.Id == user.Id);
                firstOrDefault = user;
            });
        }

        public Task CreateCustomUserAsync(CustomUser user)
        {
            return Task.Run(() => users.Add(user));
        }
    }
}