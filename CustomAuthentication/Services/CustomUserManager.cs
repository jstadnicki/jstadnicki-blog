using Microsoft.AspNet.Identity;

namespace CustomAuthentication.Controllers
{
    public class CustomUserManager : UserManager<CustomUser>
    {
        public CustomUserManager(IUserStore<CustomUser> store) : base(store)
        {
        }

        public override bool SupportsUserLockout => false;
        public override bool SupportsUserRole => false;
        public override bool SupportsUserClaim => false;
        public override bool SupportsUserSecurityStamp => false;
        public override bool SupportsUserTwoFactor => false;
    }
}