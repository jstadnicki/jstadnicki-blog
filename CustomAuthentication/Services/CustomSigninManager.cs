using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace CustomAuthentication.Controllers
{
    public class CustomSigninManager : SignInManager<CustomUser, string>
    {
        public CustomSigninManager(
            UserManager<CustomUser, string> userManager,
            IAuthenticationManager authenticationManager) :
                base(userManager, authenticationManager)
        {
        }
    }
}