using System.Threading.Tasks;

namespace CustomAuthentication.Controllers
{
    public interface ICustomDatabase
    {
        Task<CustomUser> FindCustomUserByNameAsync(string user);
        Task<CustomUser> FindCustomUserByIdAsync(string user);
        Task DeleteCustomUserAsync(CustomUser user);
        Task UpdateCustomUserAsync(CustomUser user);
        Task CreateCustomUserAsync(CustomUser user);
    }
}