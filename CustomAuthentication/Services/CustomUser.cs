using Microsoft.AspNet.Identity.EntityFramework;

namespace CustomAuthentication.Controllers
{
    public class CustomUser : IdentityUser
    {
        public CustomUser(string username, string password)
        {
            this.UserName = username;
            this.PasswordHash = password;
        }
    }
}