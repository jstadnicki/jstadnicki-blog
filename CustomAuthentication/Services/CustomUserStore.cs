using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CustomAuthentication.Controllers
{
    public class CustomUserStore :
        UserStore<CustomUser>
    {
        private readonly ICustomDatabase database;

        public CustomUserStore(ICustomDatabase database)
        {
            this.database = database;
        }


        public void Dispose()
        {
            // IOC takes care
        }

        public override Task CreateAsync(CustomUser user)
        {
            return this.database.CreateCustomUserAsync(user);
        }

        public override Task UpdateAsync(CustomUser user)
        {
            return this.database.UpdateCustomUserAsync(user);
        }

        public override Task DeleteAsync(CustomUser user)
        {
            return this.database.DeleteCustomUserAsync(user);
        }

        public override Task<CustomUser> FindByIdAsync(string userId)
        {
            return this.database.FindCustomUserByIdAsync(userId);
        }

        public override Task<CustomUser> FindByNameAsync(string userName)
        {
            return this.database.FindCustomUserByNameAsync(userName);
        }

        public override Task AddClaimAsync(CustomUser user, Claim claim)
        {
            return base.AddClaimAsync(user, claim);
        }

        public override Task AddLoginAsync(CustomUser user, UserLoginInfo login)
        {
            return base.AddLoginAsync(user, login);
        }

        public override Task AddToRoleAsync(CustomUser user, string roleName)
        {
            return base.AddToRoleAsync(user, roleName);
        }

        public override Task<CustomUser> FindAsync(UserLoginInfo login)
        {
            return base.FindAsync(login);
        }

        public override Task<CustomUser> FindByEmailAsync(string email)
        {
            return base.FindByEmailAsync(email);
        }

        public override Task<int> GetAccessFailedCountAsync(CustomUser user)
        {
            return base.GetAccessFailedCountAsync(user);
        }

        public override Task<IList<Claim>> GetClaimsAsync(CustomUser user)
        {
            return base.GetClaimsAsync(user);
        }

        public override Task<string> GetEmailAsync(CustomUser user)
        {
            return base.GetEmailAsync(user);
        }

        public override Task<bool> GetEmailConfirmedAsync(CustomUser user)
        {
            return base.GetEmailConfirmedAsync(user);
        }

        public override Task<bool> GetLockoutEnabledAsync(CustomUser user)
        {
            return base.GetLockoutEnabledAsync(user);
        }

        public override Task<DateTimeOffset> GetLockoutEndDateAsync(CustomUser user)
        {
            return base.GetLockoutEndDateAsync(user);
        }

        public override Task<IList<UserLoginInfo>> GetLoginsAsync(CustomUser user)
        {
            return base.GetLoginsAsync(user);
        }

        public override Task<string> GetPasswordHashAsync(CustomUser user)
        {
            return base.GetPasswordHashAsync(user);
        }

        public override Task<string> GetPhoneNumberAsync(CustomUser user)
        {
            return base.GetPhoneNumberAsync(user);
        }

        public override Task<bool> GetPhoneNumberConfirmedAsync(CustomUser user)
        {
            return base.GetPhoneNumberConfirmedAsync(user);
        }

        public override Task<IList<string>> GetRolesAsync(CustomUser user)
        {
            return base.GetRolesAsync(user);
        }

        public override Task<string> GetSecurityStampAsync(CustomUser user)
        {
            return base.GetSecurityStampAsync(user);
        }

        public override Task<bool> GetTwoFactorEnabledAsync(CustomUser user)
        {
            return base.GetTwoFactorEnabledAsync(user);
        }

        protected override Task<CustomUser> GetUserAggregateAsync(Expression<Func<CustomUser, bool>> filter)
        {
            return base.GetUserAggregateAsync(filter);
        }

        public override Task<bool> HasPasswordAsync(CustomUser user)
        {
            return base.HasPasswordAsync(user);
        }

        public override Task<int> IncrementAccessFailedCountAsync(CustomUser user)
        {
            return base.IncrementAccessFailedCountAsync(user);
        }

        public override Task<bool> IsInRoleAsync(CustomUser user, string roleName)
        {
            return base.IsInRoleAsync(user, roleName);
        }

        public override Task RemoveClaimAsync(CustomUser user, Claim claim)
        {
            return base.RemoveClaimAsync(user, claim);
        }

        public override Task RemoveFromRoleAsync(CustomUser user, string roleName)
        {
            return base.RemoveFromRoleAsync(user, roleName);
        }

        public override Task RemoveLoginAsync(CustomUser user, UserLoginInfo login)
        {
            return base.RemoveLoginAsync(user, login);
        }

        public override Task ResetAccessFailedCountAsync(CustomUser user)
        {
            return base.ResetAccessFailedCountAsync(user);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override Task SetEmailAsync(CustomUser user, string email)
        {
            return base.SetEmailAsync(user, email);
        }

        public override Task SetEmailConfirmedAsync(CustomUser user, bool confirmed)
        {
            return base.SetEmailConfirmedAsync(user, confirmed);
        }

        public override Task SetLockoutEnabledAsync(CustomUser user, bool enabled)
        {
            return base.SetLockoutEnabledAsync(user, enabled);
        }

        public override Task SetLockoutEndDateAsync(CustomUser user, DateTimeOffset lockoutEnd)
        {
            return base.SetLockoutEndDateAsync(user, lockoutEnd);
        }

        public override Task SetPasswordHashAsync(CustomUser user, string passwordHash)
        {
            return base.SetPasswordHashAsync(user, passwordHash);
        }

        public override Task SetPhoneNumberAsync(CustomUser user, string phoneNumber)
        {
            return base.SetPhoneNumberAsync(user, phoneNumber);
        }

        public override Task SetPhoneNumberConfirmedAsync(CustomUser user, bool confirmed)
        {
            return base.SetPhoneNumberConfirmedAsync(user, confirmed);
        }

        public override Task SetSecurityStampAsync(CustomUser user, string stamp)
        {
            return base.SetSecurityStampAsync(user, stamp);
        }

        public override Task SetTwoFactorEnabledAsync(CustomUser user, bool enabled)
        {
            return base.SetTwoFactorEnabledAsync(user, enabled);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}