﻿using System.Web;
using System.Web.Mvc;

namespace CustomAuthentication.Controllers
{
    public class LogoutController : Controller
    {
        public ActionResult Index()
        {
            Request.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}