﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace CustomAuthentication.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return this.View("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Index(string username, string password)
        {
            var inMemoryCustomDatabase = new InMemoryCustomDatabase();
            var customUserStore = new CustomUserStore(inMemoryCustomDatabase);
            var customUserManager = new CustomUserManager(customUserStore);
            var authenticationManager = this.Request.GetOwinContext().Authentication;
            var s = new CustomSigninManager(customUserManager, authenticationManager);

            var custophasher = new CusomerPasswordHasher();
            customUserManager.PasswordHasher = custophasher;

            var passwordSignIn = s.PasswordSignIn(username, password, false, false);

            if (passwordSignIn == SignInStatus.Success)
            {
                return RedirectToAction("Index", "Secret");
            }

            this.ModelState.AddModelError(string.Empty, "Failed to login.Try to register first");
            return this.View("Index");
        }
    }

    public class CusomerPasswordHasher : IPasswordHasher
    {
        public string HashPassword(string password)
        {
            return password;
        }

        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            return hashedPassword == providedPassword
                ? PasswordVerificationResult.Success
                : PasswordVerificationResult.Failed;
        }
    }
}