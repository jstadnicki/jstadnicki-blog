using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CustomAuthentication.Controllers
{
    public class RegisterController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return this.View("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Index(string username, string password)
        {
            var inMemoryCustomDatabase = new InMemoryCustomDatabase();
            var customUserStore = new CustomUserStore(inMemoryCustomDatabase);
            var customUserManager = new CustomUserManager(customUserStore);
            var authenticationManager = this.Request.GetOwinContext().Authentication;
            var s = new CustomSigninManager(customUserManager, authenticationManager);
            var user = new CustomUser(username, password);

            var identityResult = await customUserManager.CreateAsync(user);
            if (identityResult.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError(string.Empty, string.Join(",", identityResult.Errors));
            return View("Index");
        }
    }
}