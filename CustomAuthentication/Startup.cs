﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace CustomAuthentication
{
    public class Startup
    {
        public static void Configuration(IAppBuilder appBuilder)
        {
            appBuilder.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ThisCanBeAnythingYouLike",
                LoginPath = new PathString("/Login")
            });
        }
    }
}