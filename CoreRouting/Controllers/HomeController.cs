﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreRouting.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;

    public class HomeController:Controller
    {
        public string Index()
        {
            var x = RouteData.Values;
            return string.Join("\n", x.Select(t => string.Join(":", t.Key, t.Value)));
        }
    }

    public class StarController : Controller
    {
        public string Index()
        {
            var x = RouteData.Values;
            return string.Join("\n", x.Select(t => string.Join(":", t.Key, t.Value)));
        }
    }

    public class UberController : Controller
    {
        public string Index()
        {
            var x = RouteData.Values;
            return string.Join("\n", x.Select(t => string.Join(":", t.Key, t.Value)));
        }
    }
}
