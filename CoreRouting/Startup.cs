﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace CoreRouting
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services) => services.AddMvc();

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc(routes =>
                       {
                           routes.MapRoute(name: "default",
                                           template: "{controller}/{action}/{id?}", 
                                           defaults: new {controller = "Home", action = "Index"});

                           //routes.MapRoute(name: "all",
                           //                template: "{controller}/{action}/{*star}",
                           //                defaults: new { controller="Star", action="Index" });

                           routes.MapRoute(name: "uber",
                                           template: "{*star}",
                                           defaults: new { controller = "Uber", action = "Index" });
                       });
        }
    }
}
