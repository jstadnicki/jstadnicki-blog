﻿namespace BoolToEnum2
{
    using System.Collections.Generic;
    using System.Linq;

    public enum FilterType
    {
        Unknown = 0,
        Group = 1,
        Standalone = 2
    }

    public class Filter
    {
        public Filter(int id, string name, FilterType filterType)
        {
            Id = id;
            Name = name;
            FilterType = filterType;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public FilterType FilterType { get; set; }
    }

    public class DummyUsage
    {
        public void Example()
        {
            var filters = new List<Filter>();
            filters.Add(new Filter(1, "ByName", FilterType.Group));
            filters.Add(new Filter(2, "ByDistance", FilterType.Standalone));
            filters.Add(new Filter(3, "ByDate", FilterType.Group));
            filters.Add(new Filter(4, "ByAuthor", FilterType.Standalone));


            var groupFilters = filters.Where(filter => filter.FilterType == FilterType.Group);

            groupFilters = this.FindFilters(filters, FilterType.Group);
            groupFilters = this.FindFilters(filters, FilterType.Group);
        }

        private IEnumerable<Filter> FindFilters(List<Filter> filters, FilterType filterType)
        {
            return filters.Where(filter => filter.FilterType == filterType);
        }
    }
}
