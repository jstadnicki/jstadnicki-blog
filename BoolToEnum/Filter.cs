﻿namespace BoolToEnum
{
    using System.Collections.Generic;
    using System.Linq;

    public class Filter
    {
        public Filter(int id, string name, bool isGroup)
        {
            Id = id;
            Name = name;
            IsGroup = isGroup;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsGroup { get; set; }
    }

    public class DummyUsage
    {
        public void Example()
        {
            var filters = new List<Filter>();
            filters.Add(new Filter(1, "ByName", true));
            filters.Add(new Filter(2, "ByDistance", false));
            filters.Add(new Filter(3, "ByDate", isGroup: true));
            filters.Add(new Filter(4, "ByAuthor", isGroup: false));


            var groupFilters = filters.Where(filter => filter.IsGroup == true);

            groupFilters = this.FindFilters(filters, true);
            groupFilters = this.FindFilters(filters, group: true);
        }

        private IEnumerable<Filter> FindFilters(List<Filter> filters, bool group)
        {
            return filters.Where(filter => filter.IsGroup == group);
        }
    }
}
